/* Ответ на теоретический вопрос:
Обработчик события это функция, которая запускается когда наступает определенное событие и определяет поведение программы (страницы) в данном случае.
*/
const input = document.querySelector('input');

input.addEventListener('focus', function () {
    input.classList.add('js-input');
});

input.addEventListener('blur', function () {
    if (input.value < 0 || input.value === "" || isNaN(Number(input.value))) {
        if (!document.querySelector('.warning')) {
            const warning = createAndPrependElement("h2", "Please enter correct price")
            input.classList.add('js-invalidInput');
            warning.classList.add("warning");
            return;
        } else {
            input.classList.add('js-invalidInput');
            return;
        }
    }
    if (document.querySelector('.warning')) {
        document.querySelector('.warning').remove();
        input.classList.remove('js-invalidInput');
    }
    input.classList.remove('js-input');
    input.classList.add('js-blurEvent');
    const priceSpan = createAndPrependElement("span", `Текущая цена: ${input.value} <span class="remove">&otimes;</span>`);
    let br = document.createElement('br');
    priceSpan.after(br);
    priceSpan.classList.add('js-span');
    document.querySelector(".remove").onclick = function () {
        priceSpan.remove();
        br.remove();
        input.value = null;
    }
})

function createAndPrependElement(tagName, innerHTML) {
    const element = document.createElement(tagName);
    element.innerHTML = innerHTML;
    document.body.prepend(element);
    return element;
}